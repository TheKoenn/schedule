import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ScheduleService} from './schedule.service';
import timeGridPlugin from '@fullcalendar/timegrid';
import {DatePipe} from '@angular/common';
import {FullCalendarComponent} from '@fullcalendar/angular';
import {ScheduleInterface} from './model/schedule-interface';
import {NgxSmartModalComponent} from 'ngx-smart-modal';
import set = Reflect.set;

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  @Input()
  public classCode: string;

  @ViewChild('calendar', {static: false})
  public calendar: FullCalendarComponent;

  @ViewChild('viewSelect', {static: false})
  public viewSelect: HTMLSelectElement;

  @ViewChild('lessonModal', {static: false})
  public lessonModal: NgxSmartModalComponent;

  public calendarPlugins = [timeGridPlugin];

  public lessons: Array<ScheduleInterface> = [];
  public selected: ScheduleInterface;

  public settings: string;

  public events = [];

  constructor(private service: ScheduleService, private datePipe: DatePipe) {
  }

  public ngOnInit() {
    const settings = window.localStorage.getItem('class-codes');
    if (settings != null) {
      this.settings = JSON.parse(settings).join(',');

      let index = 0;
      JSON.parse(settings).forEach(classCode => {
        this.service.getSchedule(classCode)
          .subscribe(lessons => {
            lessons.forEach(lesson => {
              this.lessons.push(lesson);
              const start = new Date(lesson.starttijd);
              const end = new Date(lesson.eindtijd);
              const title = `${lesson.lokaal}:\t${lesson.commentaar}`;
              this.events = this.events.concat({title, start, end, resourceEditable: true, id: index});
              index++;
            });
          });
      });
    } else {
      this.settings = '';
    }

    setTimeout(() => {
      if (window.innerWidth < 1000) {
        this.calendar.getApi().changeView('timeGridDay');
        this.viewSelect.selectedIndex = 1;
      }

      this.calendar.eventClick.subscribe(info => {
        info.jsEvent.preventDefault();
        this.selected = this.lessons[info.event.id];
        setTimeout(() => this.lessonModal.open(), 200);
      });
    }, 1000);
  }

  public viewChange(event) {
    this.calendar.getApi().changeView('timeGrid' + event.target.value);
  }

  public settingsSave() {
    const element: HTMLInputElement = document.getElementById('class-codes') as HTMLInputElement;
    const values = element.value.split(',');
    window.localStorage.setItem('class-codes', JSON.stringify(values));
    location.reload();
  }
}
