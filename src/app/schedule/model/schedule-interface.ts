export interface ScheduleInterface {
  lokaal: string;
  starttijd: number;
  eindtijd: number;
  roosterdatum: string;
  commentaar: string;
  groepcode: string;
  docentnamen: Array<string>;
}
