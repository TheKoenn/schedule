import {Component, Input, OnInit} from '@angular/core';
import {ScheduleInterface} from '../model/schedule-interface';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  @Input()
  public lesson: ScheduleInterface;

  public timestamp: string;

  constructor(private datePipe: DatePipe) {
  }

  ngOnInit() {
    const start = new Date(this.lesson.starttijd);
    const end = new Date(this.lesson.eindtijd);
    this.timestamp = this.datePipe.transform(start, 'HH:mm') + ' - ' + this.datePipe.transform(end, 'HH:mm');
  }
}
