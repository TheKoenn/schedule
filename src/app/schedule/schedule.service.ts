import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ScheduleInterface} from './model/schedule-interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  private static readonly BASE_URL = 'https://bits.team/test/forwarding.php?url=';
  private static readonly SCHEDULE_URL = 'http://api.windesheim.nl/api/klas/CLASS_CODE/les';

  constructor(private http: HttpClient) {
  }

  public getSchedule(classCode: string): Observable<Array<ScheduleInterface>> {
    const url = ScheduleService.SCHEDULE_URL.replace('CLASS_CODE', classCode);
    return this.http
      .get<Array<ScheduleInterface>>(ScheduleService.BASE_URL + encodeURIComponent(url));
  }
}
