import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ScheduleComponent } from './schedule/schedule.component';
import {HttpClientModule} from '@angular/common/http';
import {FullCalendarModule} from '@fullcalendar/angular';
import {DatePipe} from '@angular/common';
import {NgxSmartModalModule} from 'ngx-smart-modal';
import { PopupComponent } from './schedule/popup/popup.component';

@NgModule({
  declarations: [
    AppComponent,
    ScheduleComponent,
    PopupComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FullCalendarModule,
    NgxSmartModalModule.forRoot()
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
